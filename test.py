# total number 
age = 35
nextAge = age + 1
print(nextAge)


a = 1
b = 2
c = a + b 
d = a - b

print(c)
print(d)

number1 = 1
print(number1)     # 1
number1 += 1
print(number1)     # 2
number1 += 1
print(number1)     # 3


# string

firstName = 'NGUYEN'
lastName = 'A'
fullName = firstName + ' ' + lastName
print(fullName)

age = 17
canByAlcohol = age >=18
print(canByAlcohol)

a = '1'
b = 2

# typeof string , number
c = type(a).__name__
d = type(b).__name__
e = type(c).__name__
print(c, d, e)

# function python 
def my_Function():
    print("Hello from a function")

def sum(a, b):
    return a + b
print(sum(1,2))

def sum1(a, b):
    return a + b
result = sum(10,20)
print(result)

def triple(x):
    return x * 3
print(triple(3))

def triple(x):
    return x * 3
result = triple(10)
print(result)


# string
email = 'email'
print(email)

coursesStr = 'HTML & CSS, JavaScript, ReactJS'
def strToArray(str):
    return str.split(',')
print(strToArray(coursesStr))


def getUpperCaseName(name):
    return name.upper()
print(getUpperCaseName("Nguyen van a"))
print(getUpperCaseName("Nguyen van b"))

# number
year = 10
print(year)

def isNumber(value):
    return isinstance(value, (int, float))
print(isNumber(999)) # true init là số nguyên 
print(isNumber(3.14)) # true float là số thực 


def is_number(value):
    return isinstance(value, (int, float)) and not isinstance(value, bool) and not math.isnan(value)

import math

# Test the function:
print(is_number(999))            # True
print(is_number('abc'))          # False
print(is_number('100'))          # False

print(is_number(math.nan))       # False

# array 
games = ['xe', 'ok', 'b']
print(games)

## example 1
animals = ['Monkey', 'Tiger', 'Elephant']

def getLastElement(array):
    return array[-1]
print(getLastElement(animals))

def getFirstElement(array):
    return array[0]
print(getFirstElement(animals))

## example 2 
cars = ['Honda', 'Mazda', 'Mercedes']
def joinWithCharacter(array, charactor):
    return charactor.join(array)
print(joinWithCharacter(cars, '-'))


# object
student = {
    'name': 'hoc sinh',
    'age': 18,
    'address': 'Viet Nam'
}

print(student)

class Animal:
    def __init__(self, name, leg ,speed):
        self.name = name
        self.leg = leg
        self.speed = speed
parrot = Animal('Lam', 2, 20)
print(parrot)

    
class Student:
    def __init__(self, firstName, lastName):
        self.firstName = firstName
        self.lastName = lastName
    def getFullName(self):
        return f"{self.firstName} {self.lastName}"
student = Student('Long', 'Bui')
print(student.getFullName())

from datetime import datetime
def get_next_year():
    return datetime.now().year + 1

print(get_next_year())

import random
my_list = ['a' , 'b' , 'c' , 'd' , 'e']

def getRandomItem(arr):
    randomIndex = random.randint(0, len(arr) - 1)
    return arr[randomIndex]

random_item =  getRandomItem(my_list)
print(random_item)

# if elif 
## number
def run(a):
    if a % 15 == 0:
        return 3
    elif a % 5 == 0:
        return 2
    elif a % 3 == 0:
        return 1
print(run(15)) # 3
print(run(5))  # 2
print(run(3))  # 1

## string
def list_fruits(fruits):
    result = ""

    if fruits == "Banana":
        result = "This is a Banana"
    elif fruits == "Apple":
        result = "This is a Apple"
    return result

print(list_fruits("Banana"))
print(list_fruits("Apple"))

## dictionary thay thế switch-case

def dictionary(fruits):
    fruits_dict ={
        "Banana": "This is a Banana",
        "Apple": "This is a Apple"
    }

    return fruits_dict.get(fruits, "No fruits")

print(dictionary("Banana")) # "This is a Banana"
print(dictionary("Apple"))  # "This is a Apple"
print(dictionary("Orange")) # "No fruits"


def dictionary_number(number):
    number_dict ={
        10: "This is number ten",
        20: "This is number twenty"
    }
    return number_dict.get(number, "")

print(dictionary_number(10))
print(dictionary_number(20))

# toán tử 3 ngôi 
def getCanVoteMessage(age):
    message = 'Bạn có thể bỏ phiếu' if age >= 18 else 'Bạn chưa được bỏ phiếu'
    return message

print(getCanVoteMessage(18))
print(getCanVoteMessage(15))

# python list 
this_list = ["apple", "banana", "cherry", "orange", "kiwi", "melon", "mango"]
tropical = ["mango", "pineapple", "papaya"]
print(len(this_list)) # lấy phần tử cuối cùng trong mảng
print(this_list[1]) # in thứ tự trong mảng
print(this_list[-1]) # số âm sẽ lấy phần tử cuối cùng
print(this_list[2:4]) # lấy đầu bỏ cuối
print(this_list[:4]) # bỏ phần tử cuối
print(this_list[2:]) # bor phần tử cuối
print(this_list[-4:-1]) # Ví dụ này trả về các mục từ "orange" (-4) đến, nhưng KHÔNG bao gồm "xoài" (-1):

if "banana" in this_list: # kiểm tra string có trong arr không  
    print("Yes, 'banana' is in the fruits list") 


this_list[1] = "blackcurrant" # thêm phần tử thứ tự trong mảng
print(this_list)
this_list[1:2] = ["a", "b"]  # thêm phần tử thứ tự trong mảng
print(this_list)
this_list[1:3] = ["watermelon"] # thêm phần tử ở giữa 2 vị trí 
print(this_list)
this_list.insert(2, "c") # gán phần tử ở vị trí bất kỳ 
print(this_list)
this_list.append("d") # thêm phần tử vào this_list
print(this_list)
this_list.extend(tropical) # thêm phần tử vào phần tử muốn thêm
print(this_list)
this_list.remove("apple") # xoá phần tử trong arr 
print(this_list)
this_list.pop(1) # xoá vị trí phần tử trong arr 
print(this_list)
this_list.pop() # xoá vị trí phần tử trong arr
print(this_list)
del this_list[0]
print(this_list)
this_list.clear()
print(this_list)





my_list = ["a", "b", "c", "d", "e", "f"]
print(type(my_list))



width = 15
height = 12.0
print(height/3)

words = 'His e-mail is q-lar@freecodecamp.org'
pieces = words.split()
parts = pieces[3].split('-')
n = parts[1]
print(n)

dict = {"Fri": 20, "Thu": 6, "Sat": 1} 
dict["Thu"] = 13
dict["Sat"] = 2
dict["Sun"] = 9
print(dict) # thêm và thay thế giá trị mảng 

counts = {'quincy' : 1, 'mrugesh': 42, 'beau': 100, '0':10}
print(counts.get('kris', 0)) 


names = {'chuck' :1, 'annie' :42, 'jan': 100}
for key in names:
    if names[key] > 10:
        print(key, names[key]) # lấy phần tử trong object > 10 

lst = []
for key, val in counts.items():
    newtup = (val,key)
    lst.append(newtup)
lst = sorted(lst, reverse=True)
print(lst) 






 