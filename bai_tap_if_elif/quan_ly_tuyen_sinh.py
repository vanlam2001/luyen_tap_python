def quan_ly_tuyen_sinh(diem1, diem2, diem3, diem4 , khu_vuc, doi_tuong):

    if kiem_tra_diem(diem2) and kiem_tra_diem(diem3) and kiem_tra_diem(diem4):
        tong_diem = diem2 + diem3 + diem4 + (khu_vuc + doi_tuong)
        if tong_diem >= diem1:
            print("Bạn đã đậu, Tổng điểm", tong_diem)
        else:
            print("Bạn đã rớt, Tổng điểm:", tong_diem)
    else:
        print("Bạn đã rớt, Do có điểm nhỏ hơn hoặc bằng 0")

def kiem_tra_diem(diem):
    return diem > 0


def main():
    diem1 = float(input("Nhập điểm chuẩn:"))
    khu_vuc = float(input("Nhập khu vực:"))
    doi_tuong = float(input("Nhập đối tượng:"))
    diem2 = float(input("Nhập Điểm Môn Văn:"))
    diem3 = float(input("Nhập Điểm Môn Lý:"))
    diem4 = float(input("Nhập Điểm Môn Hoá:"))
    
    ket_qua = quan_ly_tuyen_sinh(diem1, diem2, diem3, diem4, khu_vuc, doi_tuong)
    print(ket_qua)

if __name__ == "__main__":
    main()
    