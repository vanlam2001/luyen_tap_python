import os 
import sys

def tinh_chan_le():
    so1 = float(input("Nhập số thứ nhất"))
    so2 = float(input("Nhập số thứ hai"))
    so3 = float(input("Nhập số thứ ba"))
    soChan = 0
    soLe = 0

    if so1 <= 0 and so2 <= 0 and so3 <=0:
        print("Dữ liệu không hợp lệ")
    else:
        if so1 % 2 == 0:
            soChan += 1
        if so2 % 2 == 0:
            soChan += 1
        if so3 % 2 == 0:
            soChan += 1
        soLe = 3 - soChan
    print(f"Số lượng số chẳn: {soChan}")
    print(f"Số lượng số lẻ: {soLe}")

def main():
    tinh_chan_le()

if __name__ == "__main__":
    main()