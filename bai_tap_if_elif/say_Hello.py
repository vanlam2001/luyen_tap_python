# -*- coding: utf-8 -*-
import os
import sys

def say_Hello():
    user = input('Nhập tên dòng họ: ')
    hello = ""
    if user == "Ba":
        hello = "Xin chào ba"
    elif user == "Mẹ":
        hello = "Xin chào mẹ"
    elif user == "Anh":
        hello = "Xin chào anh"
    else:
        hello = "Xin chào người lạ ơi"
    return hello 

def main():
    hello_message = say_Hello()
    print(hello_message)

if __name__ == "__main__":
    main()

         

