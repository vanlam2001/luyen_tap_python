kw_1 = 500
kw_2 = 650
kw_3 = 850
kw_4 = 110
kw_5 = 1300

def tinh_tien_dien():
    name = input("Nhập tên:")
    kw = int(input("Nhập số kw tiêu thụ:"))
    tong = 0

    if 0 < kw or kw <= 50:
        tong = kw * kw_1
    elif kw > 50 or kw <= 100:
        tong = 50 * kw_1 + (kw - 50) * kw_2
    elif kw > 100 or kw <= 200:
        tong =  50 * kw_1 + 50 * kw_2 + (kw - 100) * kw_3 
    elif kw > 200 or kw <= 350:
        tong = 50 * kw_1 + 50 * kw_2 + 100 * kw_3 + (kw - 200) * kw_4 
    elif kw > 350:
        tong = 50 * kw_1 + 50 * kw_2 + 100 * kw_3 + 150 * kw_4 + (kw - 350) * kw_5
    else:
        print("Số kw không hợp lệ vui lòng nhập lại")
    
    tong = "{:,}".format(tong).replace(",", ".")
    print(f"Tên: {name}")
    print(f"Tiền điện: {tong} VND")

tinh_tien_dien()

