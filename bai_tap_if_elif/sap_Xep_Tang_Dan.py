# -*- coding: utf-8 -*-

import os
import sys

def sort_numbers():
    num1 = float(input("Nhập số thứ nhất: "))
    num2 = float(input("Nhập số thứ hai: "))
    num3 = float(input("Nhập số thứ ba: "))

    if num1 >= num2 and num1 >= num3:
        if num2 > num3 or num2 == num3:
            sorted_numbers = f"{num3} {num2} {num1}"
        else:
            sorted_numbers = f"{num2} {num3} {num1}"
    elif num2 >= num1 and num2 >= num3:
        if num1 > num3 or num1 == num3:
            sorted_numbers = f"{num3} {num1} {num2}"
        else:
            sorted_numbers = f"{num1} {num3} {num2}"
    else:  # num3 is the largest
        if num1 > num2 or num1 == num2:
            sorted_numbers = f"{num2} {num1} {num3}"
        else:
            sorted_numbers = f"{num1} {num2} {num3}"

    return sorted_numbers

def main():
    sorted_numbers = sort_numbers()
    print("Các số sau khi sắp xếp:", sorted_numbers)

if __name__ == "__main__":
    main()
            






  