import os
import sys

def doan_Hinh_Tam_Giac(a, b, c):
    if a <= 0 or b <= 0 or c <= 0:
        return "Dữ liệu không hợp lệ"
    elif a == b == c:
        return "Tam giác đều"
    elif a == b or a == c or b == c:
        return 'Tam giác cân'
    elif a * a == b * b + c * c or b * b == a * a + c * c or c * c == a * a + b * b:
        return 'Tam giác vuông'
    else:
        return 'Loại tam giác khác'

def main():
    canh1 = float(input("Nhập độ dài cạnh 1:"))
    canh2 = float(input("Nhập độ dài cạnh 2:"))
    canh3 = float(input("Nhập độ dài cạnh 3"))

    ketQua = doan_Hinh_Tam_Giac(canh1, canh2, canh3)
    print("Kết quả", ketQua)

if __name__ == "__main__":
    main()




    
