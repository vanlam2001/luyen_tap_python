# Lớp Quản Lý Sinh Viên

class SinhVien:
    def __init__(self, ho_ten, ma_sinh_vien, ngay_sinh, diem):
        self.ho_ten = ho_ten
        self.ma_sinh_vien = ma_sinh_vien
        self.ngay_sinh = ngay_sinh
        self.diem = diem

    def in_thong_tin(self):
        print(f"Họ tên: {self.ho_ten}")
        print(f"Mã sinh viên: {self.ma_sinh_vien}")
        print(f"Ngày sinh: {self.ngay_sinh}")
        print(f"Điểm: {self.diem}")

    def tinh_tuoi(self):
        from datetime import datetime
        ngay_hien_tai = datetime.now()
        ngay_sinh = datetime.strptime(self.ngay_sinh, '%Y-%m-%d')
        tuoi = ngay_hien_tai.year - ngay_sinh.year
        return tuoi

# Tạo một đối tượng SinhVien
sinh_vien = SinhVien("Nguyen Van B", "SV12345", "1998-05-10", 8.5)

# In thông tin và tính tuổi của sinh viên
sinh_vien.in_thong_tin()
tuoi = sinh_vien.tinh_tuoi()
print(f"Tuổi: {tuoi} tuổi")
