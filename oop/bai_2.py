class HinhTron:
    def __init__(self, ban_kinh, pi=3.14159):
        self.ban_kinh = ban_kinh
        self.pi = pi
    def tinh_dien_tich_hinh_tron(self):
        return self.pi * self.ban_kinh * 2

hinh_tron = HinhTron(5)
dien_tich = hinh_tron.tinh_dien_tich_hinh_tron()
print(f"Diện tích hình tròn là: {dien_tich}")