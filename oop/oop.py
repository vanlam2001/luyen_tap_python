class HinhChuNhat:
    def tinh_dien_tich(chieu_dai, chieu_rong):
        return chieu_dai * chieu_rong
    
    def tinh_chu_vi(chieu_dai, chieu_rong):
        return (chieu_dai + chieu_rong) * 2
    
dien_tich = HinhChuNhat.tinh_dien_tich(5, 3)
chu_vi = HinhChuNhat.tinh_chu_vi(5, 3)

print("Diện tích:", dien_tich)
print("Chu vi:", chu_vi)


class SinhVien:
    def hien_thi_thong_tin(ten, tuoi):
        print(f"Tên: {ten}, Tuổi: {tuoi}")
SinhVien.hien_thi_thong_tin("Nguyễn Văn A", 20)

class HocSinh:
    def hien_thi_thong_tin(ten, tuoi, diem):
        print(f"Tên: {ten}")
        print(f"Tuổi: {tuoi}")
        print(f"Điểm: {diem}")
HocSinh.hien_thi_thong_tin("Nguyễn Văn B", 10, 9.5)

class DoVat:
    def __init__(self, ten, khoi_luong, gia_tri):
        self.ten = ten
        self.khoi_luong = khoi_luong
        self.gia_tri = gia_tri

    def hien_thi_thong_tin(self):
        print(f"Tên: {self.ten}")
        print(f"Khối lượng: {self.khoi_luong}")
        print(f"Giá trị: {self.gia_tri}")

    def tinh_gia_tri_khoi_luong(self):
        return self.gia_tri / self.khoi_luong

# Tạo một danh sách đồ vật
do_vat1 = DoVat("Laptop", 2, 2000)
do_vat2 = DoVat("Máy ảnh", 1, 800)
do_vat3 = DoVat("Sách", 3, 300)
do_vat4 = DoVat("Điện thoại", 1, 1200)

do_vat_list = [do_vat1, do_vat2, do_vat3, do_vat4]

# Tìm đồ vật có tỷ lệ giá trị/khối lượng cao nhất
max_do_vat = max(do_vat_list, key=lambda x: x.tinh_gia_tri_khoi_luong())
max_do_vat.hien_thi_thong_tin()



