class DongVat:
    def __init__(self, ten, loai):
        self.ten = ten
        self.loai = loai
    def in_thong_tin(self):
        print(f"{self.ten} là một {self.loai}")
    def tieng_keu(self, keu):
        print(f"{self.ten} kêu: {keu}")

dong_vat = DongVat("Mèo", "Thú cưng")
dong_vat.in_thong_tin()
dong_vat.tieng_keu("Meow")


