class TaiKhoanNganHang:
    def __init__ (self, so_tai_khoan, so_du=0):
        self.so_tai_khoan = so_tai_khoan
        self.so_du = so_du
    def nap_tien(self, so_tien):
        self.so_du += so_tien
    def rut_tien(self, so_tien):
        if so_tien <= self.so_du:
            self.so_du -= so_tien
        else:
            print("Không đủ tiền trong tài khoản.")
    def in_so_du(self):
        print(f"Số dư tài khoản {self.so_tai_khoan}: {self.so_du} VNĐ")
    
tai_khoan = TaiKhoanNganHang("TK12345", 1000)
tai_khoan.in_so_du()
tai_khoan.nap_tien(2000)
tai_khoan.in_so_du()
tai_khoan.rut_tien(1500)
tai_khoan.in_so_du()