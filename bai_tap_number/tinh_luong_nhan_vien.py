# -*- coding: utf-8 -*-
import os
import sys

def tinh_Luong():
    luon1Ngay = float(input("Nhập lương mỗi ngày"))
    soNgayLam = float(input("Nhập số ngày làm"))
    tongLuong = luon1Ngay * soNgayLam
    return tongLuong

def main():
    tongLuong = tinh_Luong()
    print("Tổng lương", tongLuong)

if __name__ == "__main__":
    main()