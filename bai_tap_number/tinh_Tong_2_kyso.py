import os
import sys

def tinh_Tong_2kyso():
    number = float(input("Nhập số"))
    hangChuc = number // 10
    donVi = number % 10
    tong2kyso = hangChuc + donVi
    return tong2kyso

def main():
    tong2kyso = tinh_Tong_2kyso()
    print("Tổng hai ký số", tong2kyso)

if __name__ == "__main__":
    main()

   