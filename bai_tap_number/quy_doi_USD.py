# -*- coding: utf-8 -*-
import os
import sys

def quyDoi():
    usd = float(input('Nhập số tiền USD'))
    vnd = usd * 23500
    vnd_formatted = "{:,.0f}".format(vnd)
    return vnd_formatted
def main():
    vnd = quyDoi()
    print('Số tiền quy đổi được', vnd)

if __name__ == '__main__':
    main()
