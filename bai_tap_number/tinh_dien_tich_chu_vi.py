# -*- coding: utf-8 -*-
import os
import sys

def tinh_Dien_Tich_Chu_Vi():
    chieuRong = float(input("Nhập chiều rộng"))
    chieuDai = float(input("Nhập chiều dài"))
    dienTich = chieuRong * chieuRong
    chuVi = (chieuDai + chieuRong) * 2
    return dienTich, chuVi

def main():
    dienTich, chuVi = tinh_Dien_Tich_Chu_Vi()
    print(f"Diện tích: {dienTich}")
    print(f"Chu vi: {chuVi}")

if __name__ == "__main__":
    main()