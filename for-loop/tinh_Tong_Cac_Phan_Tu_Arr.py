import os
import sys



def get_total(arr):
    number = 0

    for i in arr:
        number += i
    
    return number

def main():
    arr = [4,5,3,5]
    total = get_total(arr)
    print(total)

if __name__ == "__main__":
    main()