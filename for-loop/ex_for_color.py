from colorama import Fore, Back, Style
def ex_for_color():
    text = ""
    for text_1 in range(0, 11):
        if text_1 % 2 == 0:
            text += f"{Fore.RED} số chẵn {Style.RESET_ALL}"
        else:
            text += f"{Fore.BLUE} số lẻ {Style.RESET_ALL}"
    return text 

colored_ex_for_color = ex_for_color()
print(colored_ex_for_color)

