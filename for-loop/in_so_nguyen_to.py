def check_so_nguyen_to(check):
    if check < 2:
        return False
    for so_nguyen_to in range(2, int(check ** 0.5) + 1):
        if check % so_nguyen_to == 0:
            return False
    return True 

def in_so_nguyen_to():
    n = int(input("Nhập giá trị n: "))
    so_nguyen_to = ""
    for num in range(1, n + 1):
        if check_so_nguyen_to(num):
            so_nguyen_to += " " + str(num)
    print("Các số nguyên tố từ 1 đến", n, "là:", so_nguyen_to)

in_so_nguyen_to()

