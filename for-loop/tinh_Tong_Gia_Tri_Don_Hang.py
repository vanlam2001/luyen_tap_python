import os 
import sys 

def get_total(arr):
    total = 0
    for order in arr:
        total += order['price']
    
    return total

def main():
    orders = [
    {
        'name': 'Khóa học HTML - CSS Pro',
        'price': 3000000
    },
    {
        'name': 'Khóa học Javascript Pro',
        'price': 2500000
    },
    {
        'name': 'Khóa học React Pro',
        'price': 3200000
    }
]   
    total_amout = get_total(orders)
    print(total_amout)

if __name__ == "__main__":
    main()
