# tính lương 1 tháng nhân viên
luong1NgayLam = 100000
soNgayLam = 30
luong = 0

luong = luong1NgayLam * soNgayLam
print('Lương nhân viên =',luong)

## Tính tổng giá trị trung bình và /5
numb1 = 10
numb2 = 20
numb3 = 30
numb4 = 40
numb5 = 50

tongGiaTriTrungBinh = (numb1 + numb2 + numb3 + numb4 + numb5) /5
print('Tổng giá trị trung bình =', tongGiaTriTrungBinh)

## Quy đổi tiền
giaVND = 23500
soLuongUSD = 2
gia2USD = 0

gia2USD = giaVND * soLuongUSD
print('2 USD =', gia2USD)

## Tính chi vi và diện tích
chieuDai = 5
chieuRong = 3

chuVi = 0
dienTich = 0

chuVi = (chieuDai + chieuRong) * 2
dienTich = chieuDai + chieuRong
print('Chu vi =', chuVi)
print('Diện tích =', dienTich)

## Tính tổng 2 ký số
n = 50
tong2KySo = 0

hangChuc = n // 10
donVi = n % 10
tong2KySo = hangChuc + donVi
print('Tổng 2 ký số =', tong2KySo)